﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PasswordChange : MonoBehaviour
{
    public InputField InputPass;
    public InputField InputPassAgain;

    public Text TextStatusPasswordChange_Profile;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ButtonChangePass()
    {
        StartCoroutine(API.Request(API.ACC_PASSWORD, (WWWForm form) => {
            form.AddField("password", InputPass.text);
            form.AddField("password_confirmation", InputPassAgain.text);

            
            TextStatusPasswordChange_Profile.text = "Waiting to Change Password...";

            return new Dictionary<string, string>() { { "Authorization", "Bearer " + Account.token } }; 
        }, (WWW www) => {
            Debug.Log("Login Sukses " + www.text);

            // Parsing to object
            Response.BASE_RESPONSE obj = JsonUtility.FromJson<Response.BASE_RESPONSE>(www.text);

            //Account.Inject(obj.data);
            TextStatusPasswordChange_Profile.text = obj.message;

            // Open panel dashboard

            return 0;
        }, (WWW www) => {
            Debug.Log("Login Failed " + www.text);

            Response.AUTH_CHANGE_PASSWORD_FAILURE obj = JsonUtility.FromJson<Response.AUTH_CHANGE_PASSWORD_FAILURE>(www.text);

            try
            {
                StatusHelper.Show(TextStatusPasswordChange_Profile, obj.data.password, obj.data.password_confirmation);
            }
            catch
            {
                TextStatusPasswordChange_Profile.text = obj.message;
            }

            return 1;
        }));
    }
}

