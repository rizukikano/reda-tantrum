﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour {
    public GameObject PanelLogin_Email;
    public GameObject PanelLogin_Password;
    public GameObject PanelLogin_Forgot;
    public InputField InputEmail;
    public InputField InputPassword;
    public Text TextStatusLogin_Email;
    public Text TextStatusLogin_Password;
    public Text TextStatusLogin_Forgot;
    private string next = "Main";
    public void MoveTo(string to)
    {
        if (to.Equals("email"))
        {
            PanelLogin_Email.SetActive(true);
            PanelLogin_Password.SetActive(false);
            PanelLogin_Forgot.SetActive(false);
        }
        else if (to.Equals("password"))
        {
            PanelLogin_Email.SetActive(false);
            PanelLogin_Password.SetActive(true);
            PanelLogin_Forgot.SetActive(false);
        }
        else if (to.Equals("forgot"))
        {
            PanelLogin_Email.SetActive(false);
            PanelLogin_Password.SetActive(false);
            PanelLogin_Forgot.SetActive(true);
        }
    }

    public void ButtonEmail()
    {
        
        StartCoroutine(API.Request(API.CHECK_EMAIL, (WWWForm form) => {
            form.AddField("email", InputEmail.text);
            form.AddField("expected", "available");
            
            TextStatusLogin_Email.text = "Waiting to check...";

            return new Dictionary<string, string>();
        }, (WWW www) => {
            Debug.Log("Check Sukses " + www.text);
            TextStatusLogin_Email.text = "Check Sukses";

            // Parsing to object
            Response.BASE_RESPONSE obj = JsonUtility.FromJson<Response.BASE_RESPONSE>(www.text);

            TextStatusLogin_Email.text = "";
            PanelLogin_Email.SetActive(false);
            PanelLogin_Password.SetActive(true);

            return 0;
        }, (WWW www) => {
            Debug.Log("Check Failed " + www.text);

            Response.CHECK_EMAIL_FAILURE obj = JsonUtility.FromJson<Response.CHECK_EMAIL_FAILURE>(www.text);

            
            try
            {
                StatusHelper.Show(TextStatusLogin_Email, obj.data.email, obj.data.expected);
            }
            catch
            {
                TextStatusLogin_Email.text = obj.message;
            }

            return 1;
        }));
    }

    public void ButtonLogin()
    {
        StartCoroutine(API.Request(API.AUTH_LOGIN, (WWWForm form) => {
            form.AddField("email", InputEmail.text);
            form.AddField("password", InputPassword.text);

            TextStatusLogin_Password.text = "Waiting to Login...";

            return new Dictionary<string, string>();
        }, (WWW www) => {
            Debug.Log("Login Sukses " + www.text);

            // Parsing to object
            Response.AUTH_LOGIN_SUCCESS obj = JsonUtility.FromJson<Response.AUTH_LOGIN_SUCCESS>(www.text);

            Account.Inject(obj.data);


            // Open panel dashboard
            SceneManager.LoadScene(next, LoadSceneMode.Single);

            return 0;
        }, (WWW www) => {
            Debug.Log("Login Failed " + www.text);

            Response.AUTH_LOGIN_FAILURE obj = JsonUtility.FromJson<Response.AUTH_LOGIN_FAILURE>(www.text);

            try
            {
                StatusHelper.Show(TextStatusLogin_Password, obj.data.email, obj.data.password);
            }
            catch
            {
                TextStatusLogin_Password.text = obj.message;
            }

            return 1;
        }));
    }

    public void ButtonForgot()
    {
        StartCoroutine(API.Request(API.AUTH_LOST, (WWWForm form) => {
            form.AddField("email", InputEmail.text);

            TextStatusLogin_Forgot.text = "Waiting to request...";

            return new Dictionary<string, string>();
        }, (WWW www) => {
            Debug.Log("Forgot Sukses " + www.text);

            // Parsing to object
            Response.BASE_RESPONSE obj = JsonUtility.FromJson<Response.BASE_RESPONSE>(www.text);
            TextStatusLogin_Forgot.text = obj.message;

            return 0;
        }, (WWW www) => {
            Debug.Log("Forgot Failed " + www.text);

            Response.BASE_RESPONSE obj = JsonUtility.FromJson<Response.BASE_RESPONSE>(www.text);
            TextStatusLogin_Forgot.text = obj.message;

            return 1;
        }));
    }

}
