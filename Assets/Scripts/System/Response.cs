﻿using System;
using System.Collections.Generic;

public class Response
{
    [Serializable]
    public class BASE_RESPONSE
    {
        public string status;
        public string message;
    }

    // Login Response
    [Serializable]
    public class CHECK_EMAIL_FAILURE_DATA
    {
        public List<string> email;
        public List<string> expected;
    }

    [Serializable]
    public class CHECK_EMAIL_FAILURE : BASE_RESPONSE
    {
        public CHECK_EMAIL_FAILURE_DATA data;
    }

    // Login Response
    [Serializable]
    public class AUTH_LOGIN_SUCCESS_DATA
    {
        public string name;
        public string email;
        public string birth;
        public string phone;
        public string photo;
        public string token;
        public string baby_photo;
    }

    [Serializable]
    public class AUTH_LOGIN_FAILURE_DATA
    {
        public List<string> email;
        public List<string> password;
    }

    [Serializable]
    public class AUTH_LOGIN_SUCCESS : BASE_RESPONSE
    {
        public AUTH_LOGIN_SUCCESS_DATA data;
    }

    [Serializable]
    public class AUTH_LOGIN_FAILURE : BASE_RESPONSE
    {
        public AUTH_LOGIN_FAILURE_DATA data;
    }


    // Register Response
    [Serializable]
    public class AUTH_REGISTER_FAILURE_DATA
    {
        public List<string> email;
        public List<string> password;
        public List<string> name;
        public List<string> birth;
        public List<string> phone;
        public List<string> photo;
    }

    [Serializable]
    public class AUTH_REGISTER_SUCCESS : BASE_RESPONSE
    {
    }

    [Serializable]
    public class AUTH_REGISTER_FAILURE : BASE_RESPONSE
    {
        public AUTH_REGISTER_FAILURE_DATA data;
    }

    [Serializable]
    public class AUTH_CHANGE_PASSWORD_FAILURE_DATA
    {
        public List<string> password;
        public List<string> password_confirmation;
    }

    [Serializable]
    public class AUTH_CHANGE_PASSWORD_FAILURE : BASE_RESPONSE
    {
        public AUTH_CHANGE_PASSWORD_FAILURE_DATA data;
    }

    [Serializable]
    public class TANTRUM_DETAIL_SUCCESS_DATA
    {
        public int id;
        public string counter;
        public string symptoms;
        public string note;
        public string date;
    }

    [Serializable]
    public class TANTRUM_DETAIL_SUCCESS : BASE_RESPONSE
    {
        public List<TANTRUM_DETAIL_SUCCESS_DATA> data;
    }

    [Serializable]
    public class TANTRUM_MONTHLY_SUCCESS : BASE_RESPONSE
    {
        public List<string> data;
    }

    [Serializable]
    public class DETAIL_SAVE_DATA
    {
        public List<string> counter;
        public List<string> symptoms;
        public List<string> note;
    }

    [Serializable]
    public class DETAIL_SAVE_DATA_FAILURE: BASE_RESPONSE
    {
        public DETAIL_SAVE_DATA data ;
    }
    [Serializable]
    public class DETAIL_SAVE_DATA_SUCCESS : BASE_RESPONSE
    {
        public DETAIL_SAVE_DATA data;
    }
    public class DETAIL_DELETE
    {
        public List<string> tantrum_id;
    }

    public class DETAIL_DELETE_FAIL : BASE_RESPONSE
    {
        public DETAIL_DELETE data;
    }
    public class DETAIL_DELETE_SUCCESS: BASE_RESPONSE
    {
        public DETAIL_DELETE data;
    }

}