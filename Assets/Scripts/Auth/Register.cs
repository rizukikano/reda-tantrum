﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Register : MonoBehaviour {
    public GameObject PanelRegister_Email;
    public GameObject PanelRegister_Password;
    public GameObject PanelRegister_Profile;
    public GameObject PanelRegister_Verify;
    public InputField InputEmail;
    public InputField InputPassword;
    public InputField InputPasswordConfirmation;
    public InputField InputName;
    public Text Birth;
    public InputField InputPhone;
    public ProfilePicker photo;

    public Text TextStatusRegister_Email;
    public Text TextStatusRegister_Password;
    public Text TextStatusRegister_Profile;

    public void MoveTo(string to)
    {
        if(to.Equals("email"))
        {
            PanelRegister_Email.SetActive(true);
            PanelRegister_Password.SetActive(false);
            PanelRegister_Profile.SetActive(false);
            PanelRegister_Verify.SetActive(false);
        } else if (to.Equals("password"))
        {
            PanelRegister_Email.SetActive(false);
            PanelRegister_Password.SetActive(true);
            PanelRegister_Profile.SetActive(false);
            PanelRegister_Verify.SetActive(false);
        } else if (to.Equals("profile"))
        {
            PanelRegister_Email.SetActive(false);
            PanelRegister_Password.SetActive(false);
            PanelRegister_Profile.SetActive(true);
            PanelRegister_Verify.SetActive(false);
        } else if (to.Equals("verify"))
        {
            PanelRegister_Email.SetActive(false);
            PanelRegister_Password.SetActive(false);
            PanelRegister_Profile.SetActive(false);
            PanelRegister_Verify.SetActive(true);
        }
    }

    public void ButtonEmail()
    {
        
        StartCoroutine(API.Request(API.CHECK_EMAIL, (WWWForm form) => {
            form.AddField("email", InputEmail.text);
            form.AddField("expected", "not available");
            
            TextStatusRegister_Email.text = "Waiting to check...";

            return new Dictionary<string, string>();
        }, (WWW www) => {
            Debug.Log("Check Sukses " + www.text);
            TextStatusRegister_Email.text = "Check Sukses";

            // Parsing to object
            Response.BASE_RESPONSE obj = JsonUtility.FromJson<Response.BASE_RESPONSE>(www.text);

            TextStatusRegister_Email.text = "";

            // Move to
            MoveTo("password");

            return 0;
        }, (WWW www) => {
            Debug.Log("Check Failed " + www.text);

            Response.CHECK_EMAIL_FAILURE obj = JsonUtility.FromJson<Response.CHECK_EMAIL_FAILURE>(www.text);

            try
            {
                StatusHelper.Show(TextStatusRegister_Email, obj.data.email, obj.data.expected);
            }
            catch
            {
                TextStatusRegister_Email.text = obj.message;
            }
            

            return 1;
        }));
    }

    public void ButtonPassword()
    {
        if(InputPassword.text.Equals(""))
        {
            TextStatusRegister_Password.text = "Password cannot null";
            return;
        }
        else if(!InputPassword.text.Equals(InputPasswordConfirmation.text)) {
            TextStatusRegister_Password.text = "Password confirmation not match";
            return;
        }

        MoveTo("profile");
    }

    public void ButtonRegister()
    {
        StartCoroutine(API.Request(API.AUTH_REGISTER, (WWWForm form) => {
            form.AddField("email", InputEmail.text);
            form.AddField("password", InputPassword.text);
            form.AddField("name", InputName.text);
            form.AddField("birth", Birth.text);
            form.AddField("phone", InputPhone.text);
            form.AddField("photo", photo.photo);

            TextStatusRegister_Profile.text = "Waiting to Register...";

            return new Dictionary<string, string>();
        }, (WWW www) => {
            Debug.Log("Register Sukses " + www.text);

            // Parsing to object
            Response.BASE_RESPONSE obj = JsonUtility.FromJson<Response.BASE_RESPONSE>(www.text);
            

            MoveTo("verify");

            return 0;
        }, (WWW www) => {
            Debug.Log("Register Failed " + www.text);

            Response.AUTH_REGISTER_FAILURE obj = JsonUtility.FromJson<Response.AUTH_REGISTER_FAILURE>(www.text);

            try
            {
                StatusHelper.Show(TextStatusRegister_Profile, obj.data.email, obj.data.password, obj.data.name, obj.data.birth, obj.data.phone, obj.data.photo);
            }
            catch
            {
                TextStatusRegister_Profile.text = obj.message;
            }

            return 1;
        }));
    }

}
