﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class API
{
    /* Authentication API */
    public static readonly string AUTH_LOGIN = "auth/login";
    public static readonly string AUTH_REGISTER = "auth/register";
    public static readonly string AUTH_LOST = "auth/lost-password";
    public static readonly string CHECK_EMAIL = "auth/check-email";
    public static readonly string ACC_PROFILE = "account/profile";
    public static readonly string ACC_PASSWORD = "account/password";
    public static readonly string ACC_PHOTO = "account/photo";

    public static readonly string SAVE_DATA = "tantrum/save";
    public static readonly string GET_TANTRUM_MONTHLY = "tantrum/monthly";
    public static readonly string GET_TANTRUM_DETAIL = "tantrum/lists";
    public static readonly string EDIT_DATA = "tantrum/edit";
    public static readonly string DELETE_DATA = "tantrum/delete"; 




    public static IEnumerator Request(string url, Func<WWWForm, Dictionary<string, string>> setup, Func<WWW, int> callbackSuccess, Func<WWW, int> callbackError)
    {
        WWWForm form = new WWWForm();

        // Get headers
        Dictionary<string, string> headers = setup(form);

        WWW www = new WWW(Config.API_URL + url, form.data.Length > 0 ? form.data : null, headers);
        yield return www;

        if (!string.IsNullOrEmpty(www.error))
        {
            callbackError(www);
        }
        else
        {
            callbackSuccess(www);
        }
    }
}
