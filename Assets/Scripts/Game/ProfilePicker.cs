﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfilePicker : MonoBehaviour {
    public GameObject foto1;
    public GameObject foto2;
    public GameObject foto3;
    public GameObject foto4;
    public GameObject foto5;
    public GameObject foto6;
    public GameObject current;
    public GameObject next;
    public string photo;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Button1()
    {
        current.SetActive(false);
        next.SetActive(true);
        foto1.SetActive(true);
        foto2.SetActive(false);
        foto3.SetActive(false);
        foto4.SetActive(false);
        foto5.SetActive(false);
        foto6.SetActive(false);
        photo= "1";
    }
    public void Button2()
    {
        current.SetActive(false);
        next.SetActive(true);
        foto2.SetActive(true);
        foto1.SetActive(false);
        foto3.SetActive(false);
        foto4.SetActive(false);
        foto5.SetActive(false);
        foto6.SetActive(false);
        photo = "2";
    }
    public void Button3()
    {
        current.SetActive(false);
        next.SetActive(true);
        foto3.SetActive(true);
        foto2.SetActive(false);
        foto1.SetActive(false);
        foto4.SetActive(false);
        foto5.SetActive(false);
        foto6.SetActive(false);
        photo = "3";
    }
    public void Button4()
    {
        current.SetActive(false);
        next.SetActive(true);
        foto4.SetActive(true);
        foto2.SetActive(false);
        foto3.SetActive(false);
        foto1.SetActive(false);
        foto5.SetActive(false);
        foto6.SetActive(false);
        photo= "4";

    }
    public void Button5()
    {
        current.SetActive(false);
        next.SetActive(true);
        foto5.SetActive(true);
        foto2.SetActive(false);
        foto3.SetActive(false);
        foto4.SetActive(false);
        foto1.SetActive(false);
        foto6.SetActive(false);
        photo = "5";
    }
    public void Button6()
    {
        current.SetActive(false);
        next.SetActive(true);
        foto6.SetActive(true);
        foto2.SetActive(false);
        foto3.SetActive(false);
        foto4.SetActive(false);
        foto5.SetActive(false);
        foto1.SetActive(false);
        photo = "6";
    }
}
