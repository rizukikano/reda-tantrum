﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataManager : MonoBehaviour
{
    public Text TextStatus_LoadMonthly;
    public Text TextStatus_LoadDetail;

    public GameObject monthlyItemPrefab;
    public DataTantrum detailItemPrefab1;
    public GameObject detailItemPrefab2;

    public Transform monthlyItemParent;
    public Transform detailItemParent;

    public GameObject panelMonthly;
    public GameObject panelDetail;

    public GameObject panelEdit;

    public Button edit;
    public Button delete;

    float yItemStart = 565f;
    private  int list = 0 ;
    // Use this for initialization
    void Start()
    {
    }

    void LoadMonthly(List<string> data)
    {

        for (int i = 0; i < data.Count; i++)
        {
            GameObject obj = Instantiate(monthlyItemPrefab, monthlyItemParent);
            obj.GetComponent<RectTransform>().localPosition = new Vector3(0, monthlyItemPrefab.transform.position.y - i * 315, 0);

            var temp = data[i];
            obj.GetComponentInChildren<Text>().text = temp;
            obj.GetComponent<Button>().onClick.AddListener(() => MonthlyDetailRequest(temp));
        }
    }

    void LoadMonthlyDetail(List<Response.TANTRUM_DETAIL_SUCCESS_DATA> data)
    {
        float yData1 = 0;
        float yData2 = 0;
        float diff2to2 = -705f;
        float diff1to1 = -505f;
        float diffAllien = -610f;
        float nextDiff = 0f;
        int last = 0;
        float lastY = 0f;

        for (int i = 0; i < data.Count; i++)
        {
            string[] counter = data[i].counter.Split(';');

            if (last != 0)
            {
                if (last == 1)
                {
                    if (counter.Length == 1)
                    {
                        nextDiff = diff1to1;
                    }
                    else if (counter.Length == 2)
                    {
                        nextDiff = diffAllien;
                    }
                }
                else if (last == 2)
                {
                    if (counter.Length == 1)
                    {
                        nextDiff = diffAllien;
                    }
                    else if (counter.Length == 2)
                    {
                        nextDiff = diff2to2;
                    }
                }
            }
            else
            {
                lastY = counter.Length == 1 ? yData1 : yData2;
            }

            if (counter.Length == 1)
            {
                list++;
                DataTantrum obj = Instantiate(detailItemPrefab1, detailItemParent) as DataTantrum ;
                obj.counter = counter[0];
                obj.symptomps = data[i].symptoms;
                obj.note = data[i].note;
                obj.id = data[i].id;


                lastY += nextDiff;
                obj.GetComponent<RectTransform>().localPosition = new Vector3(0, lastY, 0);

                obj.transform.GetChild(0).GetComponentInChildren<Text>().text = "Tanggal " + data[i].date;
                obj.transform.GetChild(1).GetComponentInChildren<Text>().text = "Tantrum selama " + counter[0];
                obj.transform.GetChild(2).GetComponentInChildren<Text>().text = "Yang dilakukan: " + data[i].symptoms;
                obj.transform.GetChild(4).GetComponentInChildren<Text>().text = data[i].note;

                Debug.Log("Symp: " + data[i].symptoms);

            }
            else if (counter.Length == 2)
            {
                list++;
                GameObject obj = Instantiate(detailItemPrefab2, detailItemParent);

                lastY += nextDiff;
                obj.GetComponent<RectTransform>().localPosition = new Vector3(0, lastY, 0);

                obj.transform.GetChild(0).GetComponentInChildren<Text>().text = "Tanggal " + data[i].date;
                obj.transform.GetChild(1).GetComponentInChildren<Text>().text = "Tantrum selama " + counter[0];
                obj.transform.GetChild(2).GetComponentInChildren<Text>().text = "Yang dilakukan: " + data[i].symptoms;
                obj.transform.GetChild(3).GetComponentInChildren<Text>().text = "Tantrum selama " + counter[1];
                obj.transform.GetChild(4).GetComponentInChildren<Text>().text = "Yang dilakukan: " + data[i].symptoms;
                obj.transform.GetChild(6).GetComponentInChildren<Text>().text = "Catatan";
            }

            last = counter.Length;
        }
    }

    public void MonthlyRequest()
    {
        list = 0;
        foreach (Transform t in monthlyItemParent)
        {
            t.parent = null;
        }

        StartCoroutine(API.Request(API.GET_TANTRUM_MONTHLY, (WWWForm form) =>
        {
            panelMonthly.SetActive(true);
            TextStatus_LoadMonthly.text = "Loading...";

            return new Dictionary<string, string>() { { "Authorization", "Bearer " + Account.token } };
        }, (WWW www) =>
        {
            Debug.Log("Success " + www.text);

            // Parsing to object
            Response.TANTRUM_MONTHLY_SUCCESS obj = JsonUtility.FromJson<Response.TANTRUM_MONTHLY_SUCCESS>(www.text);

            if (obj.data.Count > 0)
            {
                TextStatus_LoadMonthly.text = "";
                LoadMonthly(obj.data);
            }
            else
            {
                TextStatus_LoadMonthly.text = "No Data";
            }

            return 0;
        }, (WWW www) =>
        {
            Debug.Log("Load Monthly Failed " + www.text);

            TextStatus_LoadMonthly.text = "Failed Load Data";

            return 1;
        }));
    }

    void MonthlyDetailRequest(string txt)
    {
        panelMonthly.SetActive(false);
        panelDetail.SetActive(true);

        foreach (Transform t in detailItemParent)
        {
            //t.parent = null;
            Destroy(t.gameObject);
        }

        StartCoroutine(API.Request(API.GET_TANTRUM_DETAIL, (WWWForm form) =>
        {
            form.AddField("filter", txt);
            
            TextStatus_LoadDetail.text = "Loading...";

            return new Dictionary<string, string>() { { "Authorization", "Bearer " + Account.token } };
        }, (WWW www) =>
        {
            Debug.Log("Success " + www.text);

            // Parsing to object
            Response.TANTRUM_DETAIL_SUCCESS obj = JsonUtility.FromJson<Response.TANTRUM_DETAIL_SUCCESS>(www.text);

            if (obj.data.Count > 0)
            {
                TextStatus_LoadDetail.text = "";
                LoadMonthlyDetail(obj.data);
            }
            else
            {
                TextStatus_LoadDetail.text = "No Data";
            }

            return 0;
        }, (WWW www) =>
        {
            Debug.Log("Load Detail Failed " + www.text);

            TextStatus_LoadMonthly.text = "Failed Load Data";

            return 1;
        }));
    }




}
