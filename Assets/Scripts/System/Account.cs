﻿
public class Account {
    public static string name = "";
    public static string email = "";
    public static string birth = "";
    public static string phone = "";
    public static string photo = "";
    public static string token = "";
    public static string baby_photo = "";

    public static void Inject(Response.AUTH_LOGIN_SUCCESS_DATA temp)
    {
        name = temp.name;
        email = temp.email;
        birth = temp.birth;
        phone = temp.phone;
        photo = temp.photo;
        token = temp.token;
        baby_photo = temp.baby_photo;
    }

    public static void Clear()
    {
        name = "";
        email = "";
        birth = "";
        phone = "";
        photo  = "";
        token  = "";
        baby_photo = "";
    }

}
