﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfilChange : MonoBehaviour
{
    public InputField InputName;
    public Text Birth;
    public InputField InputPhone;
    private int maxbirth = 10;
    public GameObject defaultfoto;
    public GameObject foto1;
    public GameObject foto2;
    public GameObject foto3;
    public GameObject foto4;
    public GameObject foto5;
    public GameObject foto6;
    public Text TextStatusRegister_Profile;
    public ProfilChangePicture text;
    
    // Use this for initialization
    void Start()
    {
        InputName.text = Account.name;
        Name = Account.birth;
        Birth.text = Name;
        InputPhone.text = Account.phone;
        if (Account.photo == "1")
        {
            defaultfoto.SetActive(false);
            foto1.SetActive(true);
            foto2.SetActive(false);
            foto3.SetActive(false);
            foto4.SetActive(false);
            foto5.SetActive(false);
            foto6.SetActive(false);
        }
        else if (Account.photo == "2")
        {
            defaultfoto.SetActive(false);
            foto2.SetActive(true);
            foto1.SetActive(false);
            foto3.SetActive(false);
            foto4.SetActive(false);
            foto5.SetActive(false);
            foto6.SetActive(false);
        }
        else if (Account.photo == "3")
        {
            defaultfoto.SetActive(false);
            foto3.SetActive(true);
            foto2.SetActive(false);
            foto1.SetActive(false);
            foto4.SetActive(false);
            foto5.SetActive(false);
            foto6.SetActive(false);
        }
        else if (Account.photo == "4")
        {
            defaultfoto.SetActive(false);
            foto4.SetActive(true);
            foto2.SetActive(false);
            foto3.SetActive(false);
            foto1.SetActive(false);
            foto5.SetActive(false);
            foto6.SetActive(false);
        }
        else if (Account.photo == "5")
        {
            defaultfoto.SetActive(false);
            foto5.SetActive(true);
            foto2.SetActive(false);
            foto3.SetActive(false);
            foto4.SetActive(false);
            foto1.SetActive(false);
            foto6.SetActive(false);
        }
        else if (Account.photo == "6")
        {
            defaultfoto.SetActive(false);
            foto6.SetActive(true);
            foto2.SetActive(false);
            foto3.SetActive(false);
            foto4.SetActive(false);
            foto5.SetActive(false);
            foto1.SetActive(false);
        }

    }
   
    public string Name
    {
        get
        {

            return Birth.text;
        }
        set
        {
            Birth.text = value.Substring(0, maxbirth);
        }
    }

    // Update is called once per frame
    void Update()
    {
       

    }
    public void ButtonChange()
    {
        StartCoroutine(API.Request(API.ACC_PROFILE, (WWWForm form) =>
        {
            form.AddField("name", InputName.text);
            form.AddField("birth", Birth.text);
            form.AddField("phone", InputPhone.text);
            form.AddField("photo", text.edit);

            TextStatusRegister_Profile.text = "Waiting to Change Profile...";

            return new Dictionary<string, string>() { { "Authorization", "Bearer " + Account.token } };
        }, (WWW www) =>
        {
            Debug.Log("Success " + www.text);
            TextStatusRegister_Profile.text = "Change Success";

            // Parsing to object
            Response.BASE_RESPONSE obj = JsonUtility.FromJson<Response.BASE_RESPONSE>(www.text);

            TextStatusRegister_Profile.text = obj.message;
            Account.name = InputName.text;
            Account.birth = Birth.text;
            Account.phone = InputPhone.text;
            Account.photo = text.edit;
            Debug.Log(Account.photo);
            FindObjectOfType<ProfilManager>().Refresh();
            FindObjectOfType<ProfilMenu>().Refresh();
            FindObjectOfType<Profil>().Refresh();

            return 0;
        }, (WWW www) =>
        {
            Debug.Log("Change Failed " + www.text);

            Response.AUTH_REGISTER_FAILURE obj = JsonUtility.FromJson<Response.AUTH_REGISTER_FAILURE>(www.text);

            try
            {
                StatusHelper.Show(TextStatusRegister_Profile, obj.data.name, obj.data.birth, obj.data.phone, obj.data.photo);
            }
            catch
            {
                TextStatusRegister_Profile.text = obj.message;
            }

            return 1;
        }));
    }
}
