﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class DataSave : MonoBehaviour {
    public Text counter;
    public Toggle string1;
    public Toggle string2;
    public Toggle string3;
    public Toggle string4;
    public Toggle string5;
    public Toggle string6;
    public Toggle string7;
    public Toggle string8;
    public Toggle string9;
    public InputField note;
    private string symptomps = "";
    public Text TextStatus_SaveData;

    public Text editCounter;
    public Text editCounterBottom;
    public Toggle[] editSymptoms;
    public InputField editNote;
    public Text TextStatus_EditData;
    private int tantrum_id;

    // Use this for initialization
    void Start() {
        counter.text = "00:00:00";
        symptomps = "";
        note.text = "";

    }


    // Update is called once per frame
    void Update () {
        
	}
    public void ButtonSave()
    {
        if (string1.isOn)
        {
            symptomps = symptomps + "Menangis, ";
        }
        if (string2.isOn)
        {
            symptomps = symptomps + "Menjerit, ";
        }
        if (string3.isOn)
        {
            symptomps = symptomps + "Berteriak, ";
        }
        if (string4.isOn)
        {
            symptomps = symptomps + "Merengek, ";
        }
        if (string5.isOn)
        {
            symptomps = symptomps + "Melempar benda, ";
        }
        if (string6.isOn)
        {
            symptomps = symptomps + "Menggigit, ";
        }
        if (string7.isOn)
        {
            symptomps = symptomps + "Menyumpah, ";
        }
        if (string8.isOn)
        {
            symptomps = symptomps + "Mengkritik, ";
        }
        if (string9.isOn)
        {
            symptomps = symptomps + "Mengancam";
        }
        StartCoroutine(API.Request(API.SAVE_DATA, (WWWForm form) =>
        {
            form.AddField("counter",counter.text );
            form.AddField("symptoms", symptomps);
            form.AddField("note", " "+note.text);

            TextStatus_SaveData.text = "Waiting to Save...";

            return new Dictionary<string, string>() { { "Authorization", "Bearer " + Account.token } };
        }, (WWW www) =>
        {
            Debug.Log("Success " + www.text);
            TextStatus_SaveData.text = "Save Succes";

            // Parsing to object
            Response.BASE_RESPONSE obj = JsonUtility.FromJson<Response.BASE_RESPONSE>(www.text);

            TextStatus_SaveData.text = obj.message;
            symptomps = "";
            return 0;
        }, (WWW www) =>
        {
            Debug.Log("Save Failed " + www.text);

            Response.DETAIL_SAVE_DATA_FAILURE obj = JsonUtility.FromJson<Response.DETAIL_SAVE_DATA_FAILURE>(www.text);

            try
            {
                StatusHelper.Show(TextStatus_SaveData, obj.data.counter, obj.data.symptoms, obj.data.note );
            }
            catch
            {
                TextStatus_SaveData.text = obj.message;
            }

            return 1;
        }));
    }

    public void LoadForm(int id, string counter, string symptoms, string note)
    {
        tantrum_id = id;
        editNote.text = note;
        editCounter.text = counter;
        editCounterBottom.text = counter;

        string[] order = { "Menangis", "Menjerit", "Berteriak", "Merengek", "Melempar benda", "Menggigit", "Menyumpah", "Mengkritik", "Mengancam" };

        for (int i = 0; i < order.Length; i++)
        {
            editSymptoms[i].isOn = symptoms.Contains(order[i]) ? true : false;
        }
    }

    public void ButtonUpdate()
    {
        string newSymptoms = "";
        string[] order = { "Menangis", "Menjerit", "Berteriak", "Merengek", "Melempar benda", "Menggigit", "Menyumpah", "Mengkritik", "Mengancam" };

        int count = 0;
        for (int i = 0; i < editSymptoms.Length; i++)
        {
            if(editSymptoms[i].isOn)
            {
                newSymptoms += count == 0 ? "" : ", ";
                newSymptoms += order[i];

                count++;
            }
        }

        StartCoroutine(API.Request(API.EDIT_DATA, (WWWForm form) =>
        {
            form.AddField("tantrum_id", tantrum_id);
            form.AddField("counter", editCounter.text);
            form.AddField("symptoms", newSymptoms);
            form.AddField("note", " " + editNote.text);

            TextStatus_EditData.text = "Waiting to Edit...";

            return new Dictionary<string, string>() { { "Authorization", "Bearer " + Account.token } };
        }, (WWW www) =>
        {
            Debug.Log("Success " + www.text);
            TextStatus_EditData.text = "Save Succes";

            // Parsing to object
            Response.BASE_RESPONSE obj = JsonUtility.FromJson<Response.BASE_RESPONSE>(www.text);

            TextStatus_EditData.text = obj.message;
            symptomps = "";
            return 0;
        }, (WWW www) =>
        {
            Debug.Log("Edit Failed " + www.text);

            Response.DETAIL_SAVE_DATA_FAILURE obj = JsonUtility.FromJson<Response.DETAIL_SAVE_DATA_FAILURE>(www.text);

            try
            {
                StatusHelper.Show(TextStatus_EditData, obj.data.counter, obj.data.symptoms, obj.data.note);
            }
            catch
            {
                TextStatus_EditData.text = obj.message;
            }

            return 1;
        }));
    }
}
