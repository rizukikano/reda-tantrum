﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfilManager : MonoBehaviour {
    public Text nama;
    public Text birth;
    public Text nohp;
    private int maxbirth = 10;
    public GameObject defaultfoto;
    public GameObject foto1;
    public GameObject foto2;
    public GameObject foto3;
    public GameObject foto4;
    public GameObject foto5;
    public GameObject foto6;
    public ProfilChangePicture text;

    // Use this for initialization
    void Start () {
        Refresh();
        


    }
    public string Name
    {
        get
        {
           
            return birth.text;
        }
        set
        {
            birth.text = value.Substring(0, maxbirth);
        }
    }

    // Update is called once per frame
    public void Refresh () {
        nama.text = Account.name;
        Name = Account.birth;
        birth.text = Name;
        nohp.text = Account.phone;
        text.edit = Account.photo;

        if (Account.photo == "1")
        {
            defaultfoto.SetActive(false);

            foto1.SetActive(true);
            foto2.SetActive(false);
            foto3.SetActive(false);
            foto4.SetActive(false);
            foto5.SetActive(false);
            foto6.SetActive(false);
        }
        else if (Account.photo == "2")
        {
            defaultfoto.SetActive(false);
            foto2.SetActive(true);
            foto1.SetActive(false);
            foto3.SetActive(false);
            foto4.SetActive(false);
            foto5.SetActive(false);
            foto6.SetActive(false);
        }
        else if (Account.photo == "3")
        {
            defaultfoto.SetActive(false);
            foto3.SetActive(true);
            foto2.SetActive(false);
            foto1.SetActive(false);
            foto4.SetActive(false);
            foto5.SetActive(false);
            foto6.SetActive(false);
        }
        else if (Account.photo == "4")
        {
            defaultfoto.SetActive(false);
            foto4.SetActive(true);
            foto2.SetActive(false);
            foto3.SetActive(false);
            foto1.SetActive(false);
            foto5.SetActive(false);
            foto6.SetActive(false);
        }
        else if (Account.photo == "5")
        {
            defaultfoto.SetActive(false);
            foto5.SetActive(true);
            foto2.SetActive(false);
            foto3.SetActive(false);
            foto4.SetActive(false);
            foto1.SetActive(false);
            foto6.SetActive(false);
        }
        else if (Account.photo == "6")
        {
            defaultfoto.SetActive(false);
            foto6.SetActive(true);
            foto2.SetActive(false);
            foto3.SetActive(false);
            foto4.SetActive(false);
            foto5.SetActive(false);
            foto1.SetActive(false);
        }
    }
}
