﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Scene : MonoBehaviour {
    public Button move;
	// Use this for initialization
	void Start () {
        move.onClick.AddListener(Button);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void Button()
    {
        SceneManager.LoadScene("Main", LoadSceneMode.Single);
    }
}
