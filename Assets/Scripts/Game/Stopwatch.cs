﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stopwatch : MonoBehaviour {
    Text text;
    public Text save1;
    float theTime;
    bool playing;
    bool reset;


	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
		
	}
	
	// Update is called once per frame
	void Update () {
        if (playing == true) {
            theTime += Time.deltaTime;
            string minutes = Mathf.Floor((theTime % 3600) / 60).ToString("00");
            string seconds = (theTime % 60).ToString("00");
            string miliseconds = ((theTime * 100) % 100).ToString("00");
            text.text = minutes + ":" + seconds + ":" + miliseconds;
        }
	}

    public void ClickPlay()
    {
        playing = true;
    }
    public void ClickStop()
    {
        playing = false;
    }
    public void ClickReset ()
    {
        reset = true;
        if (reset == true)
        {
            text.text = "00:00:00";
            theTime = 0;
            save1.text = "";
        }
    }
    public void ClickSave()
    {
        string minutes = Mathf.Floor((theTime % 3600) / 60).ToString("00");
        string seconds = (theTime % 60).ToString("00");
        string miliseconds = ((theTime * 100) % 100).ToString("00");

        save1.text = minutes + ":" + seconds + ":" + miliseconds;
    }
}
