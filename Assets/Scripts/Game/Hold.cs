﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hold : MonoBehaviour  {

    public GameObject current;
    public GameObject next;
    public bool PointerDown
    {
        get;
        private set;
    }
    void Start()
    {
        PointerDown = false;
    }
    public void OnPointerDown()
    {
        PointerDown = true;
        current.SetActive(false);
        next.SetActive(true);
    }
}

