﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataTantrum : MonoBehaviour {
    public string counter;
    public string note;
    public string symptomps = "";
    public int id;
    public GameObject detaildata;
    public GameObject edit;
    public Text TextStatus_Delete;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void ButtonEdit()
    {
        FindObjectOfType<DataSave>().LoadForm(id, counter, symptomps, note);
        FindObjectOfType<DataManager>().panelDetail.SetActive(false);
        FindObjectOfType<DataManager>().panelEdit.SetActive(true);
    }
    public void ButtonDelete()
    {
        StartCoroutine(API.Request(API.DELETE_DATA, (WWWForm form) =>
        {
            form.AddField("tantrum_id",id);
            FindObjectOfType<DataManager>().TextStatus_LoadDetail.text = "Loading Data to Delete...";
            //TextStatus_Delete.text = "Loading Data to Delete...";
            return new Dictionary<string, string>() { { "Authorization", "Bearer " + Account.token } };
        }, (WWW www) =>
        {
            Debug.Log("Success " + www.text);
            //TextStatus_Delete.text = "Delete Success";

            // Parsing to object
            Response.DETAIL_DELETE_SUCCESS obj = JsonUtility.FromJson<Response.DETAIL_DELETE_SUCCESS>(www.text);
            Destroy(this.gameObject);

            FindObjectOfType<DataManager>().TextStatus_LoadDetail.text = obj.message;

            return 0;
        }, (WWW www) =>
        {
            Debug.Log("Delete Failed " + www.text);
            Response.DETAIL_DELETE_FAIL obj = JsonUtility.FromJson<Response.DETAIL_DELETE_FAIL>(www.text);
            // TextStatus_Delete.text =;
            FindObjectOfType<DataManager>().TextStatus_LoadDetail.text = obj.message;
            return 1;
        }));
    }

}
