﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class PlayVideo : MonoBehaviour {
    public RawImage satu;
    public RawImage dua;
    public RawImage tiga;
    public VideoPlayer source;
    public GameObject off1;
    public GameObject off2;
    public GameObject off3;
    public GameObject play1;
    public GameObject play2;
    public GameObject play3;
    public GameObject pause1;
    public GameObject pause2;
    // Use this for initialization
    public void StartCourutine () {
        StartCoroutine(Play());
	}
    public void ButPause()
    {
        StartCoroutine(ButtonPause());
    }
	
    IEnumerator Play()
    {
        off1.SetActive(false);
        off2.SetActive(false);
        off3.SetActive(false);
        play1.SetActive(false);
        play2.SetActive(false);
        play3.SetActive(false);
        satu.texture = source.texture;
        dua.texture = source.texture;
        tiga.texture = source.texture;
        source.Prepare();
        WaitForSeconds wts = new WaitForSeconds(0);
        while (!source.isPrepared)
        {
            yield return wts;
            break;
        }

        source.Play();

    }
    IEnumerator ButtonPause()
    {
        off1.SetActive(false);
        off2.SetActive(false);
        off3.SetActive(false);
        play1.SetActive(true);
        play2.SetActive(true);
        play3.SetActive(true);
        satu.texture = source.texture;
        dua.texture = source.texture;
        tiga.texture = source.texture;
        source.Prepare();
        WaitForSeconds wts = new WaitForSeconds(0);
        while (!source.isPrepared)
        {
            yield return wts;
            break;
        }

        source.Pause();

    }

}
