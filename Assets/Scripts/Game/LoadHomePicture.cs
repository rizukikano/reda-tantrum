﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadHomePicture : MonoBehaviour {
    public GameObject defaultfoto;
    public GameObject foto1;
    public GameObject foto2;
    public GameObject foto3;
    public GameObject foto4;
    public GameObject foto5;

    // Use this for initialization
    void Start () {
        Foto();

    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Foto()
    {
        if (Account.baby_photo == "1")
        {
            defaultfoto.SetActive(false);
            foto1.SetActive(true);
            foto2.SetActive(false);
            foto3.SetActive(false);
            foto4.SetActive(false);
            foto5.SetActive(false);
        }
        else if (Account.baby_photo == "2")
        {
            defaultfoto.SetActive(false);
            foto2.SetActive(true);
            foto1.SetActive(false);
            foto3.SetActive(false);
            foto4.SetActive(false);
            foto5.SetActive(false);
        }
        else if (Account.baby_photo == "3")
        {
            defaultfoto.SetActive(false);
            foto3.SetActive(true);
            foto2.SetActive(false);
            foto1.SetActive(false);
            foto4.SetActive(false);
            foto5.SetActive(false);
        }
        else if (Account.baby_photo == "4")
        {
            defaultfoto.SetActive(false);
            foto4.SetActive(true);
            foto2.SetActive(false);
            foto3.SetActive(false);
            foto1.SetActive(false);
            foto5.SetActive(false);
        }
        else if (Account.baby_photo == "5")
        {
            defaultfoto.SetActive(false);
            foto5.SetActive(true);
            foto2.SetActive(false);
            foto3.SetActive(false);
            foto4.SetActive(false);
            foto1.SetActive(false);
        }
    }
}
