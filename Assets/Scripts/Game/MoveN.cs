﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MoveN : MonoBehaviour {

    public GameObject current;
    public GameObject next;
    public string before = "Login";
    public Button button;
    // Use this for initialization
    void Start()
    {
        button.onClick.AddListener(ButtonClick);

    }
    public void ButtonClick()
    {
        current.SetActive(false);
        SceneManager.LoadScene(before, LoadSceneMode.Single);
        next.SetActive(true);
    }
    // Update is called once per frame
    void Update()
    {


    }
}
