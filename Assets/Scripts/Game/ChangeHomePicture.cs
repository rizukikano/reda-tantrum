﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeHomePicture : MonoBehaviour {
    public GameObject defaultfoto;
    public GameObject foto1;
    public GameObject foto2;
    public GameObject foto3;
    public GameObject foto4;
    public GameObject foto5;    
    public GameObject current;
    public GameObject next;
    public string photo;
    // Use this for initialization
    void Start () {
		
	}
    public void Button1()
    {   defaultfoto.SetActive(false);
        current.SetActive(false);
        next.SetActive(true);
        foto1.SetActive(true);
        foto2.SetActive(false);
        foto3.SetActive(false);
        foto4.SetActive(false);
        foto5.SetActive(false);
        photo = "1";
        ButtonChange();
    }
    public void Button2()
    {
        defaultfoto.SetActive(false);
        current.SetActive(false);
        next.SetActive(true);
        foto2.SetActive(true);
        foto1.SetActive(false);
        foto3.SetActive(false);
        foto4.SetActive(false);
        foto5.SetActive(false);
        photo = "2";
        ButtonChange();
    }
    public void Button3()
    {
        defaultfoto.SetActive(false);
        current.SetActive(false);
        next.SetActive(true);
        foto3.SetActive(true);
        foto2.SetActive(false);
        foto1.SetActive(false);
        foto4.SetActive(false);
        foto5.SetActive(false);
        photo = "3";
        ButtonChange();
    }
    public void Button4()
    {
        defaultfoto.SetActive(false);
        current.SetActive(false);
        next.SetActive(true);
        foto4.SetActive(true);
        foto2.SetActive(false);
        foto3.SetActive(false);
        foto1.SetActive(false);
        foto5.SetActive(false);
        photo = "4";
        ButtonChange();

    }
    public void Button5()
    {
        defaultfoto.SetActive(false);
        current.SetActive(false);
        next.SetActive(true);
        foto5.SetActive(true);
        foto2.SetActive(false);
        foto3.SetActive(false);
        foto4.SetActive(false);
        foto1.SetActive(false);
        photo = "5";
        ButtonChange();
    }
    // Update is called once per frame
    void Update () {
		
	}
    public void ButtonChange()
    {
        StartCoroutine(API.Request(API.ACC_PHOTO, (WWWForm form) =>
        {
            form.AddField("photo", photo);


            return new Dictionary<string, string>() { { "Authorization", "Bearer " + Account.token } };
        }, (WWW www) =>
        {
            Debug.Log("Success " + www.text);
            
            // Parsing to object
            Response.BASE_RESPONSE obj = JsonUtility.FromJson<Response.BASE_RESPONSE>(www.text);
            Account.baby_photo = photo;
            FindObjectOfType<LoadHomePicture>().Foto();
            return 0;
        }, (WWW www) =>
        {
            Debug.Log("Change Failed " + www.text);

            Response.BASE_RESPONSE obj = JsonUtility.FromJson<Response.BASE_RESPONSE>(www.text);


            return 1;
        }));
    }
}
