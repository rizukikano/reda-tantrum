﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wait2 : MonoBehaviour
{

    public GameObject chat1;
    public GameObject chat2;
    public GameObject chat3;
    public float speed;
    public GameObject button1;
    public GameObject button2;
    void Start()
    {
        StartCoroutine(Example());
    }

    IEnumerator Example()
    {
        chat1.SetActive(true);
        yield return new WaitForSeconds(speed);
        chat2.SetActive(true);
        yield return new WaitForSeconds(speed);
        chat3.SetActive(true);
        yield return new WaitForSeconds(speed);
        button1.SetActive(true);
        button2.SetActive(true);
    }
}
