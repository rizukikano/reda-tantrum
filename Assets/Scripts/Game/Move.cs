﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Move : MonoBehaviour {

    public GameObject current;
    public GameObject next;
    public Button button;
    // Use this for initialization
    void Start()
    {
        button.onClick.AddListener(ButtonClick);

    }

    public void ButtonClick()
    {
        current.SetActive(false);
        next.SetActive(true);

        if (next.name == "DataTantrum")
            FindObjectOfType<DataManager>().MonthlyRequest();
    }
    // Update is called once per frame
    void Update()
    {


    }
}
