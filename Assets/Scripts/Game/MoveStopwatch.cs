﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MoveStopwatch : MonoBehaviour
{

    public GameObject current;
    public GameObject next;
    public GameObject current1;
    public GameObject next1;

    public Button button;
    // Use this for initialization
    void Start()
    {
        button.onClick.AddListener(ButtonClick);

    }
    public void ButtonClick()
    {
        current.SetActive(false);
        next.SetActive(true);
        current1.SetActive(false);
        next1.SetActive(true);
    }
    // Update is called once per frame
    void Update()
    {


    }
}
