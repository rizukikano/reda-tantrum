﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusHelper {
    public static void Show(Text text, params List<string>[] msgs)
    {
        text.text = "ERROR:\n";
        for (int i = 0; i < msgs.Length; i++)
        {
            for (int j = 0; j < msgs[i].Count; j++)
            {
                text.text += "  - " + msgs[i][j] + '\n';
            }
        }
    }
}
